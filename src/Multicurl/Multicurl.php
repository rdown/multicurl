<?php
/**
 * Multicurl
 *
 * @author Robert Down <robertdown@live.com>
 * @copyright Copyright (C) 2014 Stream Monkey LLC
 *
 */

namespace Multicurl;

/**
 * Class Multicurl
 *
 * @package Multicurl
 */
class Multicurl
{

    protected $_handle;

    protected $_maxConcurrentRequests = 10;

    protected $_running = null;

    public function __construct()
    {
        $this->_handle = curl_multi_init();
    }

    public function addHandle($url, array $curlOpts = array())
    {
        $handle = curl_init($url);

        foreach ($curlOpts as $k => $v) {
            curl_setopt($handle, $k, $v);
        }

        curl_multi_add_handle($this->_handle, $handle);
    }

    public function execute()
    {
        $active = null;

        do {
            $mrc = curl_multi_exec($this->_handle, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($this->_handle) != -1) {
                do {
                    $mrc = curl_multi_exec($this->_handle, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);
            }
        }
    }

    public function getContent()
    {
        $tmp = array();

        foreach ($this->_handle as $handle) {
            $tmp[] = curl_multi_getcontent($handle);
        }

        return $tmp;
    }

    public function __destruct()
    {
        curl_multi_close($this->_handle);
    }
}
